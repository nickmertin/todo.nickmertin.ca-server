#!/bin/sh
rm build/libs/todo.nickmertin.ca-server.jar
./gradlew shadowJar
scp build/libs/todo.nickmertin.ca-server.jar root@192.168.0.2:/var/www-api/todo/
ssh root@192.168.0.2 "
    systemctl restart www-api-todo
    journalctl -fu www-api-todo
"
