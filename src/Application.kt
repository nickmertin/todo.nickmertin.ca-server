package ca.nickmertin.todo.server

import ca.nickmertin.todo.server.jsonModel.*
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.ContentNegotiation
import io.ktor.gson.gson
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.response.header
import io.ktor.response.respond
import io.ktor.response.respondText
import io.ktor.routing.*
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
import org.joda.time.LocalDateTime
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import org.mindrot.jbcrypt.BCrypt
import java.io.File
import java.security.MessageDigest
import java.text.DateFormat

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

typealias PatchProjects = Map<String, Any>
typealias PatchLists = Map<String, Any>
typealias PatchItems = Map<String, Any>

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    Database.connect(
        System.getenv("SQL_CONN") ?: "",
        driver = "com.mysql.jdbc.Driver",
        user = System.getenv("SQL_USER") ?: "",
        password = System.getenv("SQL_PASS") ?: ""
    )
    install(ContentNegotiation) {
        gson {
            setDateFormat(DateFormat.LONG)
            if (testing)
                setPrettyPrinting()
        }
    }
    routing {
        post("/login") {
            val body = require<PostLogin>()
            val result = transaction {
                try {
                    val u = Users.select { Users.username eq body.username }.firstOrNull()
                    if (u != null && BCrypt.checkpw(body.password, u[Users.password])) {
                        val token = randToken(16)
                        Session[token] = Session(u[Users.id].value, body.username)
                        mapOf("token" to token, "admin" to u[Users.admin])
                    } else null
                } catch (t: Throwable) {
                    t.printStackTrace()
                    null
                }
            }
            if (result == null)
                call.respond(HttpStatusCode.Forbidden, empty)
            else
                call.respond(result)
        }
        post("/logout") {
            Session.remove(call.request.headers["X-Auth-Token"])
            call.respond(empty)
        }
        route("/projects") {
            get {
                val s = requireAuth()
                call.respond(transaction {
                    (TodoProjects innerJoin Users)
                        .slice(TodoProjects.id, TodoProjects.name, TodoProjects.public)
                        .select {
                            Users.username eq s.username
                        }.mapAllWith(TodoProjects).toList()
                })
            }
            post {
                val s = requireAuth()
                val body = require<PostProjects>()
                call.respond(mapOf("id" to transaction {
                    TodoProjects.insertAndGetId {
                        it[owner] = Users.select { Users.username eq s.username }.first()[Users.id]
                        it[name] = body.name
                    }.value
                }))
            }
        }
        route("/projects/{id}") {
            get {
                val s = requireAuth()
                val result = require(TodoProjects[id] asUser s.id, HttpStatusCode.NotFound)
                    .mapWith(TodoProjects)
                    .toMutableMap()
                transaction {
                    result["lists"] = TodoLists.slice(TodoLists.id, TodoLists.name).select {
                        TodoLists.project eq id
                    }.mapAllWith(TodoLists).toList()
                }
                call.respond(result)
            }
            patch {
                val s = requireAuth()
                val body = require<PatchProjects>()
                val old = require(TodoProjects[id] asUser s.id)
                transaction {
                    TodoProjects.update({ TodoProjects.id eq id }) {
                        it[name] = body["name"] as? String ?: old[name]
                        it[public] = body["public"] as? Boolean ?: old[public]
                    }
                }
                call.respond(empty)
            }
            delete {
                val s = requireAuth()
                call.respond(if (transaction {
                        TodoProjects.deleteWhere {
                            (TodoProjects.id eq id) and (TodoProjects.owner eq s.id)
                        }
                    } == 0) HttpStatusCode.NotFound else HttpStatusCode.OK, empty)
            }
        }
        post("/lists") {
            val s = requireAuth()
            val body = require<PostLists>()
            require(transaction {
                TodoProjects.select {
                    (TodoProjects.owner eq s.id) and (TodoProjects.id eq body.project)
                }.count()
            }, HttpStatusCode.NotFound) {
                it != 0
            }
            call.respond(mapOf("id" to transaction {
                TodoLists.insertAndGetId {
                    it[project] = EntityID(body.project, TodoProjects)
                    it[name] = body.name
                }.value
            }))
        }
        route("/lists/{id}") {
            get {
                val s = requireAuth()
                val result = require(TodoLists[id] asUser s.id, HttpStatusCode.NotFound)
                    .mapWith(TodoLists)
                    .toMutableMap()
                transaction {
                    result["items"] = TodoItems.mainSlice.select {
                        TodoItems.list eq id
                    }.mapAllWith(TodoItems.mainSlice).toList()
                }
                call.respond(result)
            }
            patch {
                val s = requireAuth()
                val body = require<PatchLists>()
                val old = require(TodoLists[id] asUser s.id)
                transaction {
                    TodoLists.update({ TodoLists.id eq id }) {
                        it[name] = body["name"] as? String ?: old[name]
                    }
                }
                call.respond(empty)
            }
            delete {
                val s = requireAuth()
                require(TodoLists[id] asUser s.id)
                call.respond(if (transaction {
                        TodoLists.deleteWhere {
                            TodoLists.id eq id
                        } == 0
                    }) HttpStatusCode.NotFound else HttpStatusCode.OK, empty)
            }
        }
        post("/items") {
            val s = requireAuth()
            val body = require<PostItems>()
            require(transaction {
                (TodoLists innerJoin TodoProjects).select {
                    (TodoProjects.owner eq s.id) and (TodoLists.id eq body.list)
                }.count()
            }, HttpStatusCode.NotFound) {
                it != 0
            }
            call.respond(mapOf("id" to transaction {
                TodoItems.insertAndGetId {
                    it[list] = EntityID(body.list, TodoLists)
                    it[text] = body.text
                    it[dueDate] = body.due_date.parseDate()
                    it[dueTime] = body.due_time?.parseDate(DateTimeFormat.forPattern("hh:mm a"))
                }.value
            }))
        }
        route("/items/{id}") {
            patch {
                val s = requireAuth()
                val body = require<PatchItems>()
                val old = require(transaction {
                    (TodoItems[id] asUser s.id)?.mapWith(TodoItems.mainSlice)
                })
                transaction {
                    TodoItems.update({ TodoItems.id eq id }) {
                        it[text] = (body["text"] ?: old["text"]) as String
                        it[dueDate] = ((body["due_date"] ?: old["due_date"]) as? String)?.parseDate()!!
                        it[dueTime] = ((body["due_time"] ?: old["due_time"]) as? String)?.parseDate(DateTimeFormat.forPattern("hh:mm a"))
                        it[done] = (body["done"] ?: old["name"]) as Boolean
                    }
                }
                call.respond(empty)
            }
            delete {
                val s = requireAuth()
                require(TodoItems[id] asUser s.id)
                call.respond(if (transaction {
                        TodoItems.deleteWhere {
                            TodoItems.id eq id
                        } == 0
                    }) HttpStatusCode.NotFound else HttpStatusCode.OK, empty)
            }
        }
        route("/feeds") {
            get {
                val s = requireAuth()
                call.respond(transaction {
                    (TodoFeeds innerJoin Users)
                        .slice(TodoFeeds.id, TodoFeeds.hash, TodoFeeds.label)
                        .select {
                            Users.username eq s.username
                        }.mapAllWith(TodoFeeds).map {
                            val m = it.toMutableMap()
                            m["hash"] = (m["hash"] as ByteArray).toHex()
                            m
                        }.toList()
                })
            }
            post {
                val s = requireAuth()
                val body = require<PostFeeds>()
                call.respond(mapOf("id" to transaction {
                    TodoFeeds.insertAndGetId {
                        it[owner] = Users.select { Users.username eq s.username }.first()[Users.id]
                        it[hash] = MessageDigest.getInstance("SHA-512").digest("${System.nanoTime()}:${body.label}:${File("/dev/urandom").readText()}".toByteArray())
                        it[label] = body.label
                    }.value
                }))
            }
        }
        route("/feeds/{id}") {
            get {
                val s = requireAuth()
                val result = require(TodoFeeds[id] asUser s.id, HttpStatusCode.NotFound)
                    .mapWith(TodoFeeds)
                    .toMutableMap()
                transaction {
                    result["hash"] = (result["hash"] as ByteArray).toHex();
                    result["projects"] = (TodoFeedProjectMap innerJoin TodoProjects)
                        .slice(TodoProjects.id, TodoProjects.name).select {
                            TodoFeedProjectMap.feed eq id
                    }.mapAllWith(TodoFeedProjectMap innerJoin TodoProjects).toList()
                    val listsSlice = (TodoFeedListMap innerJoin TodoLists innerJoin TodoProjects)
                        .slice(TodoLists.id, TodoLists.name.alias("list_name"), TodoProjects.name.alias("project_name"))
                    result["lists"] = listsSlice.select {
                        TodoFeedListMap.feed eq id
                    }.mapAllWith(listsSlice).toList()
                }
                call.respond(result)
            }
            patch {
                val s = requireAuth()
                val body = require<PatchFeeds>()
                require(TodoFeeds[id] asUser s.id)
                transaction {
                    if (body.label != null) {
                        TodoFeeds.update({ TodoFeeds.id eq id }) {
                            it[label] = body.label
                        }
                    }
                    if (body.projects != null) {
                        val projects = body.projects forTable TodoProjects
                        TodoFeedProjectMap.deleteWhere {
                            (TodoFeedProjectMap.feed eq id) and (TodoFeedProjectMap.project notInList projects)
                        }
                        TodoFeedProjectMap.insertIgnore(TodoProjects.slice(intParam(id), TodoProjects.id).select {
                            (TodoProjects.owner eq s.id) and (TodoProjects.id inList projects)
                        })
                    }
                    if (body.lists != null) {
                        val lists = body.lists forTable TodoLists
                        TodoFeedListMap.deleteWhere {
                            (TodoFeedListMap.feed eq id) and (TodoFeedListMap.list notInList lists)
                        }
                        TodoFeedListMap.insertIgnore((TodoLists innerJoin TodoProjects).slice(intParam(id), TodoLists.id).select {
                            (TodoProjects.owner eq s.id) and (TodoLists.id inList lists)
                        })
                    }
                }
                call.respond(empty)
            }
            delete {
                val s = requireAuth()
                call.respond(if (transaction {
                        TodoFeeds.deleteWhere {
                            (TodoFeeds.id eq id) and (TodoFeeds.owner eq s.id)
                        }
                    } == 0) HttpStatusCode.NotFound else HttpStatusCode.OK, empty)
            }
        }
        get ("/feed-ics/{hash}") {
            val hash = require(call.parameters["hash"]).fromHex()
            val feed = require(transaction {
                TodoFeeds.slice(TodoFeeds.id, TodoFeeds.label).select {
                    TodoFeeds.hash eq hash
                }.firstOrNull()
            }, HttpStatusCode.NotFound)
            call.respondText("" +
                    "BEGIN:VCALENDAR\r\n" +
                    "VERSION:2.0\r\n" +
                    "PRODID:todo.nickmertin.ca\r\n" +
                    "METHOD:PUBLISH\r\n" +
                    "REFRESH-INTERVAL;VALUE=DURATION:PT5M\r\n" +
                    "NAME:${feed[TodoFeeds.label]}\r\n" +
                    "X-WR-CALNAME:${feed[TodoFeeds.label]}\r\n" +
                    transaction {
                TodoFeedItems.slice(*TodoFeedItems.mainCols).select {
                    TodoFeedItems.feed eq feed[TodoFeeds.id]
                }.mapAllWith(TodoFeedItems.mainSlice).joinToString("") {
                    val allDay = it["due_time"] == null
                    "" +
                            "BEGIN:VEVENT\r\n" +
                            "UID:ical-${feed[TodoFeeds.id]}-${it["id"]}@todo.nickmertin.ca\r\n" +
                            "DTSTAMP:${DateTime.now(DateTimeZone.UTC).toString(icsDateTime)}\r\n" +
                            "DTSTART:${LocalDateTime.parse(
                                it["due_date"] as String +
                                        if (allDay) ""
                                        else " ${it["due_time"]}",
                                DateTimeFormat.forPattern(
                                    if (allDay) "yyyy-MM-dd"
                                    else "yyyy-MM-dd HH:mm:ss"
                                )).toDateTime().toString(
                                if (allDay) icsDate
                                else icsDateTime
                            )}\r\n" +
                            "SUMMARY: ${(it["text"] as String)
                                .replace("\r", "\\r").replace("\n", "\\n")}\r\n" +
                            "END:VEVENT\r\n"
                }
            } + "END:VCALENDAR\r\n", ContentType("text", "calendar"))
        }
    }
}
