package ca.nickmertin.todo.server

import java.time.LocalDateTime
import java.time.Period
import java.util.*
import kotlin.collections.HashMap
import kotlin.concurrent.timer

class Session(val id: Int, val username: String) {
    companion object : HashMap<String, Session>() {
        private val clearCacheTimer = timer("session-clear-cache", true, 0, 3600000) {
            val threshold = LocalDateTime.now() - Period.ofDays(1)
            filter {
                it.value.lastUsed < threshold
            }.forEach {
                remove(it.key)
            }
        }

        /**
         * Returns the value to which the specified key is mapped,
         * or `null` if this map contains no mapping for the key.
         *
         *
         * More formally, if this map contains a mapping from a key
         * `k` to a value `v` such that `(key==null ? k==null :
         * key.equals(k))`, then this method returns `v`; otherwise
         * it returns `null`.  (There can be at most one such mapping.)
         *
         *
         * A return value of `null` does not *necessarily*
         * indicate that the map contains no mapping for the key; it's also
         * possible that the map explicitly maps the key to `null`.
         * The [containsKey][.containsKey] operation may be used to
         * distinguish these two cases.
         *
         * @see .put
         */
        override fun get(key: String): Session? {
            val s = super.get(key)
            if (s != null)
                s.lastUsed = LocalDateTime.now()
            return s
        }
    }

    private var lastUsed = LocalDateTime.now()
}