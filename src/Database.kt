package ca.nickmertin.todo.server

import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.Function
import org.jetbrains.exposed.sql.transactions.transaction
import org.joda.time.DateTime

data class UserSensitiveQuery<T>(val delegate: (Int)->T) {
    infix fun asUser(userId: Int) = delegate(userId)
}


object Users : IntIdTable("user") {
    val username = varchar("username", 16).uniqueIndex("UNIQUE")
    val password = varchar("password", 255)
    val createTime = datetime("create_time")
    val admin = bool("admin")
}

object TodoProjects : IntIdTable("todo_project") {
    val owner = integer("owner").entityId() references Users.id
    val name = varchar("name", 255)
    val public = bool("public")
    operator fun get(projectId: Int) = UserSensitiveQuery { userId: Int ->
        transaction {
            slice(id, name, public).select {
                (owner eq userId) and (id eq projectId)
            }.firstOrNull()
        }
    }
}

object TodoLists : IntIdTable("todo_list") {
    val project = integer("project").entityId() references TodoProjects.id
    val name = varchar("name", 255)
    operator fun get(listId: Int) = UserSensitiveQuery { userId: Int ->
        transaction {
            innerJoin(TodoProjects).slice(id, name).select {
                (TodoProjects.owner eq userId) and (id eq listId)
            }.firstOrNull()
        }
    }
}

object TodoItems : IntIdTable("todo_item") {
    val list = integer("list").entityId() references TodoLists.id
    val text = text("text")
    val dueDate = datetime("due_date")
    val dueTime = datetime("due_time").nullable()
    val done = bool("done")
    val mainCols = arrayOf(id, text, DateFormat(dueDate, "%Y-%m-%d").alias("due_date"), TimeFormat(dueTime, "%l:%i %p").alias("due_time"), done)
    val mainSlice = slice(*mainCols)
    operator fun get(itemId: Int) = UserSensitiveQuery { userId: Int ->
        transaction {
            innerJoin(TodoLists).innerJoin(TodoProjects).slice(*mainCols).select {
                (TodoProjects.owner eq userId) and (id eq itemId)
            }.firstOrNull()
        }
    }
}

object TodoFeeds : IntIdTable("todo_feed") {
    val owner = integer("owner").entityId() references Users.id
    val hash = binary("hash", 64)
    val label = varchar("label", 255)
    operator fun get(projectId: Int) = UserSensitiveQuery { userId: Int ->
        transaction {
            slice(id, owner, hash, label).select {
                (owner eq userId) and (id eq projectId)
            }.firstOrNull()
        }
    }
}

object TodoFeedListMap : Table("todo_feed_list") {
    val feed = integer("feed").entityId() references TodoFeeds.id
    val list = integer("list").entityId() references TodoLists.id
}

object TodoFeedProjectMap : Table("todo_feed_project") {
    val feed = integer("feed").entityId() references TodoFeeds.id
    val project = integer("project").entityId() references TodoProjects.id
}

object TodoFeedItems : Table("todo_feed_item") {
    val id = integer("id")
    val feed = integer("feed").entityId() references TodoFeeds.id
    val text = text("text")
    val dueDate = datetime("due_date")
    val dueTime = datetime("due_time").nullable()
    val listName = varchar("list_name", 255)
    val projectName = varchar("project_name", 255)
    val mainCols = arrayOf(id, text, DateFormat(dueDate, "%Y-%m-%d").alias("due_date"), TimeFormat(dueTime, "%H:%i:%s").alias("due_time"), listName, projectName)
    val mainSlice = slice(*mainCols)
}

class DateFormat<T: DateTime?>(val expr: Expression<T>, val format: String): Function<DateTime>(DateColumnType(false)) {
    override fun toSQL(queryBuilder: QueryBuilder): String = "DATE_FORMAT(${expr.toSQL(queryBuilder)}, \"${format.replace("\"", "\\\"")}\")"
}

class TimeFormat<T: DateTime?>(val expr: Expression<T>, val format: String): Function<DateTime>(DateColumnType(false)) {
    override fun toSQL(queryBuilder: QueryBuilder): String = "TIME_FORMAT(${expr.toSQL(queryBuilder)}, \"${format.replace("\"", "\\\"")}\")"
}
