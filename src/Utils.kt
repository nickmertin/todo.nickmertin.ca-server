package ca.nickmertin.todo.server

import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.pipeline.PipelineContext
import io.ktor.request.receiveOrNull
import io.ktor.response.respond
import kotlinx.coroutines.cancel
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.*
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import java.util.Random
import kotlin.streams.asSequence

val empty = emptyMap<String, Any>()

val icsDateTime = DateTimeFormat.forPattern("yyyyMMdd'T'HHmmss'Z'")!!.withZoneUTC()
val icsDate = DateTimeFormat.forPattern("yyyyMMdd")!!.withZoneUTC()

const val TokenChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

fun randToken(length: Long) = Random().ints(length, 0, TokenChars.length).asSequence().map(TokenChars::get).joinToString("")

suspend fun PipelineContext<Unit, ApplicationCall>.fail(status: HttpStatusCode = HttpStatusCode.BadRequest) {
    call.respond(status, empty)
    coroutineContext.cancel()
}

suspend fun PipelineContext<Unit, ApplicationCall>.requireAuth(): Session {
    val session = {
        val token = call.request.headers["X-Auth-Token"]
        if (token == null) null
        else Session[token]
    }()
    if (session == null) {
        call.respond(HttpStatusCode.Forbidden, empty)
        coroutineContext.cancel()
    }
    return session!!
}

suspend inline fun <reified T : Any> PipelineContext<Unit, ApplicationCall>.require(status: HttpStatusCode = HttpStatusCode.BadRequest) = require(call.receiveOrNull<T>(), status)

suspend inline fun <T> PipelineContext<Unit, ApplicationCall>.require(value: T?, status: HttpStatusCode = HttpStatusCode.BadRequest): T {
    if (value == null) fail(status)
    return value!!
}

suspend inline fun <T> PipelineContext<Unit, ApplicationCall>.require(value: T, status: HttpStatusCode = HttpStatusCode.BadRequest, predicate: (T)->Boolean): T {
    if (!predicate(value)) fail(status)
    return value
}

fun ResultRow.mapWith(fields: FieldSet) = fields.fields.filter {
    hasValue(it)
}.map {
    when (it) {
        is Column<*> -> it.name
        is ExpressionAlias<*> -> it.alias
        else -> it.toSQL(QueryBuilder(false))
    } to if (this[it] is EntityID<*>) (this[it] as EntityID<*>).value else this[it]
}.toMap()

fun Iterable<ResultRow>.mapAllWith(fields: FieldSet) = map { it.mapWith(fields) }

infix fun IntArray.forTable(table: IntIdTable) = map { EntityID(it, table) }

val PipelineContext<Unit, ApplicationCall>.id
    get() = call.parameters["id"]!!.toInt()

fun String.parseDate() = DateTime.parse(this)

fun String.parseDate(formatter: DateTimeFormatter) = if (this.isEmpty()) null else DateTime.parse(this, formatter)

fun ByteArray.toHex() = joinToString("") { String.format("%02X", it) }

fun String.fromHex() = (0 until length / 2).map {
    substring(it * 2, it * 2 + 2).toInt(16).toByte()
}.toByteArray()
