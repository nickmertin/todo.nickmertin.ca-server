package ca.nickmertin.todo.server.jsonModel

data class PostLogin(val username: String, val password: String)
