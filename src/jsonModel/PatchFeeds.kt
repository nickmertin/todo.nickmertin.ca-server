package ca.nickmertin.todo.server.jsonModel

data class PatchFeeds(val label: String?, val projects: IntArray?, val lists: IntArray?)
