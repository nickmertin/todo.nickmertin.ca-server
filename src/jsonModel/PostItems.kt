package ca.nickmertin.todo.server.jsonModel

data class PostItems(val list: Int, val text: String, val due_date: String, val due_time: String?)
