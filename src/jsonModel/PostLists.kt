package ca.nickmertin.todo.server.jsonModel

data class PostLists(val project: Int, val name: String)
